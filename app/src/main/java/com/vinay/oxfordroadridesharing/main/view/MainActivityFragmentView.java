package com.vinay.oxfordroadridesharing.main.view;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Vinay Nikhil Pabba on 27-02-2016.
 */
public interface MainActivityFragmentView {

    void moveToLocation(LatLng latLng);

}
