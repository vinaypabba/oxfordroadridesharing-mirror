package com.vinay.oxfordroadridesharing.utils;

/**
 * Created by Vinay Nikhil Pabba on 30-01-2016.
 */
public class Constants {

    /* -----------   Other Constants   ----------- */



    /* -----------   Login Constants   ----------- */
    public static final String FIREBASE_REF = "https://orrs.firebaseio.com/";
    public static final String MY_PREF = "MyPref";
    public static final int RESET = 100;
    public static final int CHANGE = 1000;
    public static final String PROVIDER_FACEBOOK = "facebook";
    public static final String PROVIDER_PASSWORD = "password";
    public static final String keyHash = "8XW4Tm11KKGeQYhWHr95gtgY4Ws=";

    public static final double LONDON_LAT = 51.5072;
    public static final double LONDON_LNG = -0.1300;
    public static final int LOCATION_ZOOM_LEVEL = 18;
    public static final int REQUEST_CODE_LOCATION = 2;

}
